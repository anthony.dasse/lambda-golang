require (
	github.com/aws/aws-lambda-go v1.36.1
	gitlab.com/anthony.dasse/lambda-golang/core/voting v0.0.0-20230124103541-ce9a0c57ddec // indirect
)

replace gitlab.com/anthony.dasse/lambda-golang/core/voting => ../../../core/voting

replace gopkg.in/yaml.v2 => gopkg.in/yaml.v2 v2.2.8

module gitlab.com/anthony.dasse/lambda-golang/lambda/hello-world


go 1.16
