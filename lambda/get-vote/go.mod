module gitlab.com/anthony.dasse/lambda-golang/lambda/get-vote

go 1.19

require (
	github.com/aws/aws-lambda-go v1.37.0 // indirect
	gitlab.com/anthony.dasse/lambda-golang/core/voting v0.0.0-20230124101004-771703e4b013 // indirect
)


replace gitlab.com/anthony.dasse/lambda-golang/core/voting => ../../core/voting