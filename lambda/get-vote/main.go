package main

import (
	"fmt"

	voting "gitlab.com/anthony.dasse/lambda-golang/core/voting/pkg"
)

func main() {

	t := voting.Result{}
	t.Date = "2022"
	t.Votes = 14
	res, _ := t.FormatToBusiness("GET-VOTE")
	fmt.Printf("TEST --> %s\n\r", res)
}
