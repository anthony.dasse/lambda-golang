package pkg

import (
	"errors"
	"fmt"
)

type Result struct {
	Date  string
	Votes int
}

func (r *Result) FormatToBusiness(target string) (string, error) {

	if target != "" {
		return fmt.Sprintf("[%s]--[%d]::[%s]", r.Date, r.Votes, target), nil
	}

	return "", errors.New("math: square root of negative number")

}
